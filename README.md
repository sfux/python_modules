# python_modules

This script is creating a formated list of all Python packages that are available on the Leonhard cluster. The list encapsulated in a <div></div> tag that creates a 4 column table like shown at

https://scicomp.ethz.ch/wiki/Leonhard_applications_and_libraries
