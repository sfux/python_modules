#!/bin/bash

#####################################################################
# Script to create a list of all available Python packages on Euler #
# Author: Samuel Fux @ETHZ 2021                                     #
#####################################################################

# location to store the list of modules that were installed with PIP
LIST_PATH=/nfs/nas22.ethz.ch/fs2201/id_sis_hpc_group/wiki/auto/spack_module_tables/euler_python/piplist

echo -e "==Python packages on Euler==\n" > $LIST_PATH
echo -e "List of all Python packages available on Euler" >> $LIST_PATH

source /cluster/apps/local/env2lmod.sh

# GCC 4.8.5
echo -e "===GCC 4.8.5===\n" >> $LIST_PATH
for i in $( module --redirect -t avail python | tail -n +2 | tr "\n" " " )
do
        module load $i > /dev/null
        echo -e "====$i====" >> $LIST_PATH
        echo -e "<div style=\"column-count:4;-moz-column-count:4;-webkit-column-count:4\">" >> $LIST_PATH
        pip list | tail -n +3 | awk '$0="*"$0' 2>/dev/null 1>> $LIST_PATH
        echo -e "</div>\n" >> $LIST_PATH
        module unload $i > /dev/null
done

# GCC 6.3.0
module load gcc/6.3.0 > /dev/null
echo -e "===GCC 6.3.0===\n" >> $LIST_PATH
for i in $( module --redirect -t avail python | tail -n +2 | tr "\n" " " )
do
        module load $i > /dev/null
        echo -e "====$i====" >> $LIST_PATH
        echo -e "<div style=\"column-count:4;-moz-column-count:4;-webkit-column-count:4\">" >> $LIST_PATH
        pip list | tail -n +3 | awk '$0="*"$0' 2>/dev/null 1>> $LIST_PATH
        echo -e "</div>\n" >> $LIST_PATH
        module unload $i > /dev/null
done

# GCC 8.2.0
module load gcc/8.2.0 > /dev/null
echo -e "===GCC 8.2.0===\n" >> $LIST_PATH
for i in $( module --redirect -t avail python | tail -n +2 | tr "\n" " " )
do
        module load $i > /dev/null
        echo -e "====$i====" >> $LIST_PATH
        echo -e "<div style=\"column-count:4;-moz-column-count:4;-webkit-column-count:4\">" >> $LIST_PATH
        pip list | tail -n +3 | awk '$0="*"$0' 2>/dev/null 1>> $LIST_PATH
        echo -e "</div>\n" >> $LIST_PATH
        module unload $i > /dev/null
done

# load again GCC 4.8.5
module load gcc/4.8.5 > /dev/null

